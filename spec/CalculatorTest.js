var EC = protractor.ExpectedConditions;
var calculator=require('../json/CalculatorPage.json');
var calculatorPage=require('../pages/CalculatorPage.js');
var basePage=require('../pages/BasePage.js');
var grid=require('../grid/GridResultTable.js');
var utility=require('../util/CommonUtility.js');

describe('Way2Automation calculator testing', function() {
    beforeEach(function(){
        browser.get(calculator.url);
        browser.manage().timeouts().implicitlyWait(5000);
    });

    it('Validate multiple calculations', function() {
        var data=utility.getAllExcelData("Calculator",1);
        var a=data[0];
        var b=data[1];

        //step a
        calculatorPage.getTextboxFirstNumber().sendKeys(a);
        calculatorPage.getTextboxSecondNumber().sendKeys(b);
        calculatorPage.getButtonGo().click();
        basePage.waitForVisibilityOf(grid.getColumnValue("Time"));
        var expectedResult=utility.getResultValue(a,b,"addition");
        basePage.getTextOfElement(calculatorPage.getResult()).then(function (result) {
           expect(result).toBe(expectedResult);
        });

        //step b
        basePage.getTextOfElement(grid.getColumnValue("Result")).then(function (result) {
            expect(result).toBe(expectedResult);
        });

        //step c
        basePage.getTextOfElement(grid.getColumnTime()).then(function (result) {
            expect(result).toBe("Time");
        });
        basePage.getTextOfElement(grid.getColumnExpression()).then(function (result) {
            expect(result).toBe("Expression");
        });
        basePage.getTextOfElement(grid.getColumnResult()).then(function (result) {
            expect(result).toBe("Result");
        });

        //step d
        var currentTime= utility.getCurrentTime();
        basePage.getTextOfElement(grid.getColumnValue("Time")).then(function (result) {
            expect(result).contains(currentTime);
        });

        //step e
        basePage.getTextOfElement(grid.getColumnValue("Expression")).then(function (result) {
            expect(result).toBe(a+" + "+b);
        });

        //step f
        basePage.getTextOfElement(grid.getColumnValue("Result")).then(function (result) {
            expect(result).toBe(expectedResult);
        });

        //step g
        calculatorPage.performFiveOperations(a,b);
        grid.getRows().then(function (rows) {
            expect(rows.length).toBe(6);
       });

    });
});