var basePage=require('../pages/BasePage.js');
var welcomePage=require('../pages/WelcomePage.js');
var homePage=require('../pages/HomePage.js');
var welcome=require('../json/WelcomePage.json');
var utility=require('../util/CommonUtility.js');

describe('Way2Automation login testing', function() {
	beforeEach(function(){
		basePage.navigateToUrl(welcome.url);
		browser.manage().timeouts().implicitlyWait(5000);
	});

	it('Successful login test', function() {
		var data=utility.getAllExcelData("LoginData",1);
		welcomePage.getTextboxUsername().sendKeys(data[0]);
		welcomePage.getTextboxPassword().sendKeys(data[1]);
		welcomePage.getTextboxUsernameMandatory().sendKeys(data[2]);
		welcomePage.getButtonLogin().click();
		basePage.waitForVisibilityOf(homePage.getYouAreLoggedIn());
		basePage.getTextOfElement(homePage.getYouAreLoggedIn()).then(function(result){
			expect(result).toBe("You're logged in!!");
		});
	});

	it('Login test negative scenario', function() {
		var data=utility.getAllExcelData("LoginData",2);
		welcomePage.getTextboxUsername().sendKeys(data[0]);
		welcomePage.getTextboxPassword().sendKeys(data[1]);
		welcomePage.getTextboxUsernameMandatory().sendKeys(data[2]);
		welcomePage.getButtonLogin().click();
		basePage.waitForVisibilityOf(welcomePage.getUsernamePasswordIncorrect());
		basePage.getTextOfElement(welcomePage.getUsernamePasswordIncorrect()).then(function(result){
			expect(result).toBe("Username or password is incorrect");
		});
	});
});






