var EC = protractor.ExpectedConditions;
var basePage=require('../pages/BasePage.js');
var profilePage=require('../json/ProfilePage.json');
var profile=require('../pages/ProfilePage.js');
var interestsPage=require('../pages/InterestsPage.js');
var paymentPage=require('../pages/PaymentPage.js');
var commonUtility=require('../util/CommonUtility.js');

describe('Way2Automation profile testing', function() {
    beforeEach(function(){
        basePage.navigateToUrl(profilePage.url);
        browser.manage().timeouts().implicitlyWait(5000);
    });

    it('Validate new profile', function() {
        var data=commonUtility.getAllExcelData("ProfileData",1);
        //step a
        profile.getTextboxName().sendKeys(data[0]);
        profile.getTextboxEmail().sendKeys(data[1]);
        basePage.getTextOfElement(profile.getTextarea()).then(function(result){
            expect(result).toBeJsonString();
        });

        //step b
        profile.getButtonNextSection().click();
        basePage.waitForVisibilityOf(interestsPage.getLabelConsoleOfChoice());
        expect(interestsPage.getLabelConsoleOfChoice().isDisplayed()).toBe(true);

        //step c
        basePage.getAttributeOfElement(basePage.getInterests(),"class").then(function(result){
            expect(result).toBe("active");
        });

        //step d
        interestsPage.getRadiobutton().click();
        basePage.getTextOfElement(interestsPage.getTextarea()).then(function(text){
            expect(text).toBeJsonString();
        });

        //step e
        interestsPage.getButtonNextSection().click();
        basePage.waitForVisibilityOf(paymentPage.getLabelTestCompleted());
        expect(paymentPage.getLabelTestCompleted().isDisplayed()).toBe(true);

        //step f
        basePage.getAttributeOfElement(basePage.getPayment(),"class").then(function(result){
            expect(result).toBe("active");
        });

        //step g
        basePage.getTextOfElement(paymentPage.getLabelTestCompleted()).then(function(text){
            expect(text).toBe("Test Completed, WooHoo!");
        });

        //step h
        paymentPage.getButtonSubmit().click();
        var alert=commonUtility.getAlert();
        basePage.getTextOfElement(alert).then(function(text){
            expect(text).toBe("awesome!");
        });
        alert.accept();

        //step i
        basePage.getAttributeOfElement(basePage.getPayment(),"class").then(function (text) {
            expect(text).toBe("active");
        });
        basePage.getNumberTwo().click();
        basePage.getAttributeOfElement(basePage.getInterests(),"class").then(function (text) {
            expect(text).toBe("active");
        });
        basePage.getNumberOne().click();
        basePage.getAttributeOfElement(basePage.getProfile(),"class").then(function (text) {
            expect(text).toBe("active");
        });

        //step j
        basePage.getNumberTwo().click();
        basePage.waitForVisibilityOf(interestsPage.getLabelConsoleOfChoice());
        basePage.getAttributeOfElement(basePage.getProfile(),"class").then(function (text) {
            expect(text).toBe("");
        });
    });
});