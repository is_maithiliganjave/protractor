var utility=require('../util/CommonUtility.js');
var webtable=require('../json/WebTablesPage.json');
var webtablePage=require('../pages/WebTablesPage.js');
var addUserPage=require('../pages/AddUserPage.js');
var grid=require('../grid/GridWebTables.js');

describe('Way2Automation WebTables testing', function() {
    beforeEach(function(){
        browser.get(webtable.url);
        browser.manage().timeouts().implicitlyWait(5000);
    });

    it('Add,edit and delete user record scenario', function() {
        var userData1=utility.getAllExcelData("WebTableData",1);
        var userData2=utility.getAllExcelData("WebTableData",2);
        var userData3=utility.getAllExcelData("WebTableData",3);
        var userData4=utility.getAllExcelData("WebTableData",4);

        //step a
        var firstName=utility.getSplitString(userData1[0],":");
        webtablePage.getButtonAddUser().click();
        addUserPage.fillForm(userData1);
        addUserPage.getButtonSave().click();
        grid.getRowCells().then(function (rowText) {
            expect(rowText[0]).toBe(firstName[0]);
        });

        //step b
        grid.getButtonEdit().click();
        addUserPage.getTextboxFirstName().clear();
        addUserPage.getTextboxFirstName().sendKeys(firstName[1]);
        addUserPage.getButtonSave().click();
        grid.getRowCells().then(function (rowText) {
            expect(rowText[0]).toBe(firstName[1]);
        });

        //step c
        webtablePage.getSearchBox().sendKeys(userData1[6]);
        grid.getRows().then(function(rows){
            expect(rows.length).toBe(1);
        });
        webtablePage.getSearchBox().clear();

        //step d
        var expectedColumnHeaders=["First Name","Last Name","User Name","Customer","Role","E-mail","Cell Phone","Locked"];
        grid.getColumnHeaders().then(function(columnHeaders){
            columnHeaders.toContain(expectedColumnHeaders);
        });

        //step e
        expect(grid.getCheckBoxLocked().isEnabled()).toBe(false);
        grid.getButtonEdit().click();
        addUserPage.getCheckBoxLocked().click();
        addUserPage.getButtonSave().click();
        expect(grid.getCheckBoxLocked().isSelected()).toBe(true);
        expect(grid.getCheckBoxLocked().isEnabled()).toBe(false);

        //step f
        grid.getIconDelete().click();
        webtablePage.getButtonOk().click();
        browser.navigate().refresh();
        grid.getRowCells().then(function (rowText) {
            expect(rowText[0]).not.toBe(firstName[1]);
        });

        // step g
        webtablePage.getButtonAddUser().click();
        addUserPage.fillForm(userData1);
        addUserPage.getButtonSave().click();
        webtablePage.getButtonAddUser().click();
        addUserPage.fillForm(userData2);
        addUserPage.getButtonSave().click();
        webtablePage.getButtonAddUser().click();
        addUserPage.fillForm(userData3);
        addUserPage.getButtonSave().click();
        webtablePage.getButtonAddUser().click();
        addUserPage.fillForm(userData4);
        addUserPage.getButtonSave().click();
        webtablePage.getPagintion().then(function(pagination){
            expect(pagination).toBe("2");
        });
    });
});