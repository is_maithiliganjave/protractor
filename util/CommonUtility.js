var excel= require('xlsx');
var workbook=excel.readFile('./data/TestData.xlsx');

var CommonUtility=function() {

    this.getAlert = function () {
        var alert=browser.switchTo().alert();
        return alert;
    };

    this.getResultValue = function (firstValue,secondValue,operation) {
        var firstValueInteger=parseInt(firstValue);
        var secondValueInteger=parseInt(secondValue);
        switch(operation) {
            case "addition":
                return (firstValueInteger+secondValueInteger).toString();
            case "subtraction":
                return (firstValueInteger-secondValueInteger).toString();
            case "multiplication":
                return (firstValueInteger*secondValueInteger).toString();
            case "division":
                return (firstValueInteger/secondValueInteger).toString();
            case "modulus":
                return (firstValueInteger%secondValueInteger).toString();
            default:
                console.log("Invalid operation");
        }
    };

    this.getAllExcelData = function (sheetname,rowNumber) {
        var data=new Array();
        var worksheet=workbook.Sheets[sheetname];
        for (z in worksheet) {
            if(z.endsWith((rowNumber+1).toString())) {
                data.push(worksheet[z].v);
            }
        }
        return data;
    };

    this.getCurrentTime = function () {
       return new Date().toLocaleTimeString();
    };

    this.getSplitString = function (stringToSplit, seperator) {
        return stringToSplit.split(seperator);
    };
}
module.exports=new CommonUtility();



