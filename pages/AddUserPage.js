var adduser=require('../json/AddUserPage.json');
var utility=require('../util/CommonUtility.js');

var AddUserPage=function() {

    this.getTextboxFirstName = function () {
        return element(by.name(adduser.locators.firstName));
    };

    this.getTextboxLastName = function () {
        return element(by.name(adduser.locators.lastName));
    };

    this.getTextboxUserName = function () {
        return element(by.name(adduser.locators.userName));
    };

    this.getTextboxPassword = function () {
        return element(by.name(adduser.locators.password));
    };

    this.getDropDownRole = function () {
        return element(by.xpath(adduser.locators.dropDownRole));
    };

    this.getTextboxEmail = function () {
        return element(by.name(adduser.locators.email));
    };

    this.getTextboxCellPhone = function () {
        return element(by.name(adduser.locators.cellphone));
    }

    this.getButtonSave = function () {
        return $(adduser.locators.buttonSave);
    };

    this.fillForm = function (userData) {
        this.getTextboxFirstName().clear();
        this.getTextboxFirstName().sendKeys(utility.getSplitString(userData[0],":")[0]);
        this.getTextboxLastName().clear();
        this.getTextboxLastName().sendKeys(userData[1]);
        this.getTextboxUserName().clear();
        this.getTextboxUserName().sendKeys(userData[2]);
        this.getTextboxPassword().clear();
        this.getTextboxPassword().sendKeys(userData[3]);
        this.getDropDownRole().click();
        this.getDropDownRole().sendKeys(userData[4]);
        this.getTextboxEmail().clear();
        this.getTextboxEmail().sendKeys(userData[5]);
        this.getTextboxCellPhone().clear();
        this.getTextboxCellPhone().sendKeys(userData[6]);
    };

    this.getCheckBoxLocked = function () {
        return element(by.xpath(adduser.locators.checkBoxLocked));
    };
}
module.exports=new AddUserPage();