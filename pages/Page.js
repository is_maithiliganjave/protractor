var EC = protractor.ExpectedConditions;
var BasePage=function(){

    this.navigateToUrl=function(url){
        browser.get(url);
    };

    this.waitForVisibilityOf=function(element){
       // browser.sleep(6000);
        browser.wait(EC.visibilityOf(element));
    }

    this.getInterests = function () {
        return element(by.xpath("//a[text()=' Interests']"));
    };

    this.getProfile = function () {
        return  element(by.xpath("//a[text()=' Profile']"));
    };

    this.getPayment = function () {
        return  element(by.xpath("//a[text()=' Payment']"));
    };

    this.getNumberTwo = function () {
        return  element(by.xpath("//span[text()='2']"));
    };

    this.getNumberOne = function () {
        return  element(by.xpath("//span[text()='1']"));
    };

    this.getAttributeOfElement=function(element,attribute){
        var promise=new Promise(function(resolve,reject){
            resolve(element.getAttribute(attribute));
        });
        return promise;
    };

    this.getTextOfElement=function(element){
        var promise=new Promise(function(resolve,reject){
            resolve(element.getText());
        });
        return promise;
    };
}
module.exports=new BasePage();