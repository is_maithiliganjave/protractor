var webtable=require('../json/WebTablesPage.json');


var WebTablesPage=function() {

    this.getButtonAddUser=function(){
        return $(webtable.locators.buttonAddUser);
    };

    this.getSearchBox=function(){
        return element(by.model(webtable.locators.searchBox));
    };

    this.getPagintion = function () {
        var promise=new Promise(function(resolve,reject){
            resolve(element(by.xpath(webtable.locators.pagination)).getText());
        });
        return promise;
    };

    this.getButtonOk=function(){
        return $(webtable.locators.buttonOk);
    };
}
module.exports=new WebTablesPage();