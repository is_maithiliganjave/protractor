var payment=require('../json/PaymentPage.json');
var PaymentPage=function() {

    this.getLabelTestCompleted = function () {
        return element(by.tagName(payment.locators.labelTestCompleted));
    };

    this.getButtonSubmit = function () {
        return element(by.buttonText(payment.locators.buttonSubmit));
    };

}
module.exports=new PaymentPage();