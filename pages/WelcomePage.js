var welcomePage=require('../json/WelcomePage.json');
var WelcomePage=function(){

    this.getTextboxUsername=function(){
        return element(by.id(welcomePage.locators.textboxUsername));
    };

    this.getTextboxPassword=function(){
        return element(by.id(welcomePage.locators.textboxPassword));
    };

    this.getTextboxUsernameMandatory=function(){
        return element(by.model(welcomePage.locators.textboxUsernameMandatory));
    };

    this.getButtonLogin=function(){
        return element(by.buttonText(welcomePage.locators.buttonLogin));
    };

    this.getUsernamePasswordIncorrect=function(){
        return element(by.xpath(welcomePage.locators.usernamePasswordIncorrect));
    };

};
module.exports=new WelcomePage();