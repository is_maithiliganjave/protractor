var calculator=require('../json/CalculatorPage.json');
var GridTable=require('../grid/GridResultTable.js')

var CalculatorPage=function() {

    this.getTextboxFirstNumber = function () {
        return element(by.model(calculator.locators.firstNumber));
    };

    this.getTextboxSecondNumber = function () {
        return element(by.model(calculator.locators.secondNumber));
    };

    this.getButtonGo = function () {
        return element(by.id(calculator.locators.goButton));
    };

    this.getResult = function () {
        return $(calculator.locators.result);
    };

    this.performFiveOperations = function (a,b) {
        var i=5;
        do{
            this.getTextboxFirstNumber().sendKeys(a);
            this.getTextboxSecondNumber().sendKeys(b);
            this.getButtonGo().click();
            i--;
        }while(i>=1);
    };
}
module.exports=new CalculatorPage();
