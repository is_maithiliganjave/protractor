var interests=require('../json/InterestsPage.json');
var ProfilePage=function() {

    this.getLabelConsoleOfChoice= function () {
        return $(interests.locators.labelConsoleOfChoice);
    };

    this.getTextarea = function () {
        return $(interests.locators.textarea);
    };

    this.getButtonNextSection = function () {
        return element(by.linkText(interests.locators.buttonNextSection));
    };

    this.getRadiobutton = function () {
        return  element(by.xpath("//input[@type='radio']"));
    };

}
module.exports=new ProfilePage();