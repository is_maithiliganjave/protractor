// conf.js
// exports.config = {
//  specs: ['./spec/LoginTest.js'],
//  framework: 'jasmine',
//  seleniumAddress: 'http://localhost:4444/wd/hub',
//  directConnect: true
//}
 var HTMLReport = require('protractor-html-reporter-2');
 var jasmineReporters = require('jasmine-reporters');

//An example configuration file
exports.config = {
  // The address of a running selenium server.
  seleniumAddress: 'http://localhost:4444/wd/hub',
  directConnect: true,
  // Capabilities to be passed to the webdriver instance.
  capabilities: {
    browserName: 'chrome'
  },

  specs: ['./spec/WebTablesTest.js','./spec/LoginTest.js','./spec/CalculatorTest.js','./spec/ProfileTest.js'],
  framework: 'jasmine2',

  // Options to be passed to Jasmine-node.
  jasmineNodeOpts: {
    showColors: true, // Use colors in the command line report.
      defaultTimeoutInterval: 30000
  },

  plugins: [{
      package: 'jasmine2-protractor-utils',
      disableHTMLReport: true,
      disableScreenshot: false,
      screenshotPath:'./screenshots',
      screenshotOnExpectFailure:false,
      screenshotOnSpecFailure:true,
      clearFoldersBeforeTest: true
    }],

  onPrepare: function() {
    browser.ignoreSynchronization=true;
    browser.driver.manage().window().maximize();

    //html reporter2
	    jasmine.getEnv().addReporter(new jasmineReporters.JUnitXmlReporter({
	    	consolidateAll : true,
	    	savePath : './',
	    	filePrefix : 'xmlresults'
	    }));
  },

  onComplete: function() {
	     var browserName, browserVersion;
	     var capsPromise = browser.getCapabilities();

	     capsPromise.then(function (caps) {
	        browserName = caps.get('browserName');
	        browserVersion = caps.get('version');
	        platform = caps.get('platform');

	        var HTMLReport = require('protractor-html-reporter-2');

	        testConfig = {
	            reportTitle: 'Protractor Test Execution Report',
	            outputPath: './',
	            outputFilename: 'ProtractorTestReport',
	            screenshotPath: './screenshots',
	            testBrowser: browserName,
	            browserVersion: browserVersion,
	            modifiedSuiteName: false,
	            screenshotsOnlyOnFailure: false,
	            testPlatform: platform
	        };
	        new HTMLReport().from('xmlresults.xml', testConfig);
	    });
  }


};
