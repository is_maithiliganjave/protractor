var webtable=require('../json/GridWebTables.json');


var GridWebTables=function() {

    this.getRowCells = function () {
        var promise=new Promise(function(resolve,reject){
            resolve(element.all(by.xpath(webtable.locators.rowCells)).getText());
        });
        return promise;
    };

    this.getRows = function () {
        var promise=new Promise(function(resolve,reject){
            resolve($$(webtable.locators.rows).getText());
        });
        return promise;
    };

    this.getColumnHeaders = function () {
        var promise=new Promise(function(resolve,reject){
            resolve($$(webtable.locators.columnHeaders).getText());
        });
        return promise;
    };

    this.getButtonEdit=function(){
        return element(by.xpath(webtable.locators.buttonEdit));
    };

    this.getIconDelete=function(){
        return element(by.xpath(webtable.locators.iconDelete));
    };

    this.getCheckBoxLocked=function(){
        return element(by.xpath(webtable.locators.checkBoxLocked));
    };
}
module.exports=new GridWebTables();