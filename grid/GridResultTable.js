var gridResult=require('../json/GridResultTable.json');
var calculatorPage=require('../pages/CalculatorPage.js');
var GridResultTable=function(){

    this.getColumnValue = function (column) {
        switch(column.trim()) {
            case "Time":
                return element(by.xpath(gridResult.locators.resultValue+"[1]"));
            case "Expression":
                return element(by.xpath(gridResult.locators.resultValue+"[2]"));
            case "Result":
                return element(by.xpath(gridResult.locators.resultValue+"[3]"));
            default:
                console.log("Invalid column");
        }
    };

    this.getColumnTime = function () {
        return element(by.xpath(gridResult.locators.columnTime));
    };

    this.getColumnExpression = function () {
        return element(by.xpath(gridResult.locators.columnExpression));
    };

    this.getColumnResult = function () {
        return element(by.xpath(gridResult.locators.columnResult));
    };

    this.getColumns = function () {
        var elements =new Array();
        elements=element(by.xpath(gridResult.locators.parentRow)).all(by.xpath(gridResult.locators.columnHeader));
        return elements;
    };

    this.getRows = function () {
        var promise=new Promise(function(resolve,reject){
            resolve(element.all(by.css(gridResult.locators.rows)).getText());
        });
        return promise;
    };


    this.getTextOfAllElements=function(ListOfElements){
        var promise=new Promise(function(resolve,reject){
            var columnValue=new Array();
            for(var i=0;i<ListOfElements;i++)
            {
                i.getText().trim().then(function(text) {
                    columnValue.push(text);
                });
            }
        });
        return promise;
    };

}
module.exports=new GridResultTable();